#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Oct 10 14:44:45 2018

@author: armitage
"""

import numpy as np
import matplotlib.pyplot as plt
import peakutils # https://zenodo.org/badge/latestdoi/102883046 pip install peakutils
from scipy import interpolate
import meshio # https://doi.org/10.5281/zenodo.1173115 pip install meshio

# Domain dimensions
lx = 400e3
ly = 100e3

# Read in water flux
filename = 'DAC51/RUN5/rivers00200000.vtk'
mesh = meshio.read(filename)
q = np.array(mesh.point_data['discharge'])

lines = 20
x     = np.linspace(0,ly/4,lines)
tol    = 0.1*lx # avoid hitting points outside the domain
xout  = np.linspace(0+tol,lx-tol,100)

# Calculate valley spacing from peak to peak in water flux

wavelength51 = np.zeros(len(x))
for i in range(lines-1) :
    indx = np.argwhere((mesh.points[:,0] > x[i]) & (mesh.points[:,0] < x[i+1]))
    q_line = interpolate.interp1d(mesh.points[indx,1].squeeze(),q[indx].squeeze())
      
    indexes = peakutils.indexes(q_line(xout),thres=0.05,min_dist=5)
    if len(indexes) > 1 :
        wavelength51[i] = sum(np.diff(xout[indexes]))/(len(indexes)-1)
    else :
        wavelength51[i] = 0
  
    plt.plot(xout,q_line(xout),'k',linewidth=2)
    plt.plot(xout[indexes],q_line(xout)[indexes],'+r')
    i += 1

plt.figure()
plt.xlabel('Distance (km)')
plt.ylabel('Water Flux (m/yr)')
#watername = 'water_flux_spacing.svg'
#plt.savefig(watername,format='svg')
#plt.clf()



# Read in water flux
filename = 'DAC101/RUN5/rivers00200000.vtk'
mesh = meshio.read(filename)
q = np.array(mesh.point_data['discharge'])

lines = 20
x     = np.linspace(0,ly/4,lines)
tol    = 0.1*lx # avoid hitting points outside the domain
xout  = np.linspace(0+tol,lx-tol,100)

# Calculate valley spacing from peak to peak in water flux

wavelength101 = np.zeros(len(x))
for i in range(lines-1) :
    indx = np.argwhere((mesh.points[:,0] > x[i]) & (mesh.points[:,0] < x[i+1]))
    q_line = interpolate.interp1d(mesh.points[indx,1].squeeze(),q[indx].squeeze())
      
    indexes = peakutils.indexes(q_line(xout),thres=0.05,min_dist=5)
    if len(indexes) > 1 :
        wavelength101[i] = sum(np.diff(xout[indexes]))/(len(indexes)-1)
    else :
        wavelength101[i] = 0
  
    plt.plot(xout,q_line(xout),'k',linewidth=2)
    plt.plot(xout[indexes],q_line(xout)[indexes],'+r')
    i += 1

plt.figure()
plt.xlabel('Distance (km)')
plt.ylabel('Water Flux (m/yr)')

ys = [wavelength51[wavelength51>0]/lx,wavelength101[wavelength101>0]/lx]
plt.figure()
plt.boxplot(ys)
plt.ylim((0,0.15))
plt.xlabel('Resolution')
plt.ylabel('Normalised wavelength')
plotname = 'wavelengths_dac.svg'
plt.savefig(plotname,format='svg')
plt.show()